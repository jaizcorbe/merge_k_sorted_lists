package coding.exercise;

import java.util.List;

public interface Merger {
    List<Integer> merge(List<List<Integer>> lists);
}
